# -*- coding: utf-8 -*-

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# October 2021

# Install library if necessary
!pip install pyspark

"""**Spark UI**

You can monitor your App and job execution through Spark UI at http://localhost:4040/

Running Spark
"""

# Create a spark context 
from pyspark import SparkContext
sc = SparkContext("local[2]", "First App")

# Stopping Spark context 
sc.stop()

# Configuration 
# * to use all capacity
from pyspark import SparkConf, SparkContext
conf = (SparkConf()
  .setMaster("local[*]") 
  .setAppName("My app") 
  .set("spark.executor.memory", "1g"))
sc = SparkContext(conf = conf)

# Inspect 
sc.pythonVer # retrieve python version

sc.master # master url to connect to

str(sc.sparkHome) # path where psark is installed on worker nodes

str(sc.sparkUser()) # retrieve name of the spark user running sparkcontext

sc.appName # return application name

sc.applicationId # retrieve application ID

sc.defaultParallelism # return defaut level of parallelism

sc.defaultMinPartitions # default min number of partitions for RDDs

"""**Using the shell**

In the pyspark shell, a special interpreter aware sparkcontext is already created in the variable called sc

$./bin/spark-shell--master local[2]

$./bin/pyspark--master local[4] --py-files code.py

Set which master the context connects to with the --master argument, and add python .zip, .egg or .py files to the runtime path by passing comma-separated list to --py-files

**Execution**

$./bin/spark-submit examples/src/main/python/pi.py

**Loading data with RDDs (resilient distributed datasets)**

**External data**

Read either one text file from HDFS, a local file system or any Haddop supported file system URI with textFile(), or read in a directory of text files with whole TextFiles()
"""

# Here we read the file readme from sample_data in google colab
textFile = sc.textFile("sample_data/README.md")

textFile.first()

textFile.collect()

textFile2 = sc.wholeTextFiles("sample_data/")

textFile2.first() # content of a file named anscombe contained in sample_data repo # 6 files so 6 coples

"""**Word count with pySpark**

Count the occurences of unique words in a text 
"""

# split each line into words
words = textFile.flatMap(lambda line: line.strip().split(" "))

words.collect()

# count the number of all words in the document
wordCounts = words.count()
print(wordCounts)

# count the occurrence of each word
wordCounts = words.map(lambda word: (word, 1)).reduceByKey(lambda a,b:a +b)

wordCounts.collect()

# save the result if needed
wordCounts.saveAsTextFile("./wordCounts/")

# parallelized collections
rdd1 = sc.parallelize([('a',7),('a',2),('b',2)])
rdd2 = sc.parallelize([('a',["x","y","z"]),('b',["p","r"])])
rdd3 = sc.parallelize(range(100))
rdd4 = sc.parallelize([('a',["x","y","z"]),('b',["p","r"])])

"""**Retrieving RDD information**"""

rdd1.getNumPartitions() # List the number of partitions

rdd1.count() # count RDD instances

rdd1.countByKey() # Count RDD instances by key

rdd1.countByValue() # count RDD instance by value

rdd1.collectAsMap() # return (key,value) pairs as a dictionnary

sc.parallelize([]).isEmpty() # Check wether RDD is empty

"""**Summary**"""

print(rdd3.sum()) # Sum of RDD elements
print(rdd3.max()) # Max value of RDD elements
print(rdd3.min()) # Min value of RDD elements
print(rdd3.mean()) # Mean value of RDD elements
print(rdd3.stdev()) # Standard deviation value of RDD elements
print(rdd3.variance()) # Compute variance of RDD elements
print(rdd3.histogram(3)) # Compute histogram by bins
print(rdd3.stats()) # Sumarry statistics count, mean, stdev, max and min

"""**Applying functions**"""

rdd1.map(lambda x: x+(x[1], x[0])).collect() # Apply a function to each RDD element

rdd5 = rdd1.flatMap(lambda x: x+(x[1], x[0])) # apply a flatMap function to each RDD element and flatten the result
rdd5.collect()

rdd4.flatMapValues(lambda x: x).collect() # Apply a flatMap function to each (key,value) pairs

"""**Selecting Data**"""

# Getting
rdd1.collect() # Return a list with all RDD elements

rdd1.first() # Take first RDD element

rdd1.top(2) # Take top 2 RDD elements

rdd1.take(2) # Take 2 first RDD elements

"""**Sampling**"""

rdd3.sample(True, 0.1).collect() # Return sampled subset of rdd3

"""**Filtering**"""

# Filtering
rdd1.filter(lambda x: "a" in x).collect() # Filter the RDD

rdd5.distinct().collect() # Return distinct RDD values

rdd1.keys().collect() # Return (key,value) RDD's keys

rdd1.values().collect() # RReturn (key,value) RDD's values

"""**Iterating**"""

# Iterating
def g(x): print(x)
rdd1.foreach(g) # Apply a function to all RDD elements

"""**Reshaping Data**

Reducing
"""

# Reshaping Data

# Reducing

rdd1.reduceByKey(lambda x,y : x+y).collect() # Merge the RDD values for each key

rdd1.reduce(lambda a,b: a + b) # Merge the RDD values

"""**Grouping by**"""

# Grouping by
rdd3.groupBy(lambda x: x % 2).mapValues(list).collect() # Return RDD of grouped values

rdd1.groupByKey().mapValues(list).collect() # Group RDD by key

"""**Aggregating**"""

# Aggregating
seqOp = (lambda x,y: (x[0]+y, x[1]+1))
combOp = (lambda x,y: (x[0]+y[0], x[1]+y[1]))

rdd1.aggregateByKey((0,0), seqOp,combOp).collect() # Aggregate values of each RDD key

rdd3.aggregate((0,0),seqOp,combOp) # Aggregate RDD elements of each partition and then the results

def add(x,y): return x+y
rdd3.fold(0,add) # Aggregate the elements of each partition, and then the results

# If we want product for ex prod(x,y) return x*y and fold(1,prod)

rdd1.foldByKey(0,add).collect() # Merge the values for each key

rdd3.keyBy(lambda x: x+x).collect() # Create tuples of RDD elements by applying a function

"""**Mathematical Operation**"""

# Mathematical Operations
rdd1_1 = sc.parallelize([('a',4),('a',2),('b',1)])
print(rdd1.collect())
print(rdd1_1.collect())
print(rdd1.subtract(rdd1_1).collect()) # rdd1 la base et j'enlève les éléments en commun qu'il a avec rdd1_1

rdd1_1 = sc.parallelize([('a',4)])
rdd1.subtractByKey(rdd1_1).collect() # Return each (key,value) pair of rdd1 with no matching
# Enlève tout mais par éléments de la clée en commun

print(rdd1.collect())
print(rdd2.collect())
rdd1.cartesian(rdd2).collect() # Return the cartesian product of rdd and rdd2

"""**Sort**"""

# Sort
rdd2.sortBy(lambda x: x[1]).collect() # Sort RDD by given function
# si élément 0 clée et si 1 c'est la valeur

rdd2.sortByKey().collect() # Sort (key,value) RDD by key

"""**Repartitioning**"""

# Repartitioning

rdd1.repartition(4) # New RDD with 4 partitions
# C'est pour repartitionner les données donc ici 4 partitions, par défaut c'est à 2

rdd1.coalesce(1) # Decrease the number of partitions in the RDD to 1
# cette fonction réduit le nombre de partition

"""**Saving**"""

# Saving
rdd1.saveAsTextFile("rdd1.txt") # C'est distribué donc on l'a en partition

#rdd1.saveAsHadoopFile("hdfs://namenodehost/parent/child", 'org.apache.hadoop.mapred.TextOutputFormat')

"""**Pyspark DataFrames**"""

# Pyspark DataFrames

sc.stop()

"""**Initialyzing pyspark session**"""

# Initialyzing pyspark session

from pyspark.sql import SparkSession
spark = SparkSession.builder \
      .master("local[*]") \
      .appName("Cars app") \
      .config("spark.executor.memory", "1g") \
      .getOrCreate()

"""**Creating a pyspark dataframe**"""

# Creating a pyspark dataframe
# Example 1
columns = ['ids','dogs','cats']
vals = [(1,2,0), (2,0,1)] # chaque élément représente une ligne d'ou tuple de 3 valeurs, chaque élément part dans une colonne
df = spark.createDataFrame(vals, columns)

df.show() # To vizualize the data frame

# from other data sources
# Example 2
from pyspark.sql import Row
liste = [(1,2,2),(2,0,1)]

rdd2=map(lambda x: Row(ids=int(x[0]), dogs=int(x[1]), cats=int(x[2])), liste)
df2 = spark.createDataFrame(rdd2)
df2.show()

# Example 3
from sklearn.datasets import load_breast_cancer
data = load_breast_cancer()
import pandas as pd
pdf = pd.DataFrame(data.data, columns = data.feature_names)

df3 = spark.createDataFrame(pdf)

df3.show()

# Example 4
# first download the cars.csv example file. Could be obtained from here : https://perso.tele
!wget https://perso.telecom-paristech.fr/eagan/class/igr204/data/cars.csv
carslist = spark.read.csv("cars.csv", # put here your local path to this file
                           inferSchema = True,
                           header = True,
                           sep=';')

carslist.show()

# Use of filter() for the first line
carslist = carslist.filter(carslist.Car!="STRING")

carslist.show()

"""**Some pyspark dataframes wrangling**"""

# Check the type of the dataframe
type(carslist)

# Check the names of the columns / attributes
carslist.columns

carslist.schema.names

# Check the attributes and their types / the schema
carslist.dtypes

carslist.printSchema()

# Show the first row
carslist.first()

# Show N first rows, 5 in this case
carslist.head(5)

# Same result: show N rows, 5 in this case
carslist.limit(5).show()

# Same result : show N rows, 5 in this case
carslist.show(5)
# Si on veut extraire les éléments plutôt utiliser les précédents car on retourne les éléments
# Alors que avec show c'est comme un print on retourne pas vraiment les éléments, on montre

# Same result but with an attribute selection
carslist.select('Car','Acceleration').show(5)

# Show distinct values for the attribute "Origin"
carslist.select('Origin').distinct().show()

# Grouping DF data
carslist.groupBy('Cylinders','Origin').count().show()

# Ordering
carslist.groupBy('Cylinders','Origin').count().orderBy("Cylinders").show()
# Number of cars per continent (origin) and by number of cylinders

# Ordering by descendant order
carslist.groupBy('Cylinders','Origin').count().orderBy(carslist.Cylinders.desc()).show()

# Another type of grouping DF data with crosstab
carslist.crosstab('Cylinders','Origin').show()

# Grouping DF data with filtering
carslist.filter(carslist['Cylinders']>4).groupBy('Cylinders','Origin').count().show()

from pyspark.sql.functions import *

# Aggregations : having the mean for Horsepower
# The same thing is valid for some other math function : max,min,sum,stddev,etc. See the doc
carslist.agg(avg('Horsepower')).show()
carslist.agg({'Horsepower':'avg'}).show() # same in another way
carslist.select(avg('Horsepower')).show() # same in another way

# Same thing but with a new name 
carslist.select(avg('Horsepower').alias("Average Horsepower")).show()

# Aggregation for Horsepower with a groupby on Origin
carslist.select('Horsepower','Origin').groupBy('Origin').agg(max("Horsepower").alias("max"),
                                                             avg("Horsepower").alias("average"),
                                                             min("Horsepower").alias("min")).show()

"""**Using SQL queries on pySpark dataframes**"""

carslist.createOrReplaceTempView('cars_table') # depricated api carslist.registerTempTable('cars_table')

spark.sql('select Car, Origin  from cars_table').show()

spark.sql('select distinct(Origin) from cars_table').show()

"""**Exercice : Let's do the same max, mean, min aggregation for Horsepower with a groupby on Origin using the sql query**

This is how we did it with pySpark aggregation:
carslist.select('Horsepower','Origin').groupBy('Origin').agg(max("Horsepower"),mean("Horsepower"),min("Horsepower")).show()
"""

res=spark.sql('select Origin, max(Horsepower), avg(Horsepower), min(Horsepower) from cars_table group by Origin')
res.show()

"""**Converting and exporting pySPark dataframes**"""

res1 = res.rdd # Convert df into an RDD

res2 = res.toJSON() # Convert df into a RDD of string
res2.first()

res3 = res.toPandas() # Convert to pandas # could fail for some java versions (spark 2.4 officially only supports java)

print(type(res))
print(type(res1))
print(type(res2))
print(type(res3))

# Exporting your pyspark dataframe
res.write.save("carsDF.json",format="json")

spark.stop()
